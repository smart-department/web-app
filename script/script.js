// all the super globals (not available in class folder) can be declared here

const backend = new Backend();
const getParams = Object.fromEntries(window.location.search.substring(1).split("&").map((el) => el.split("=")))

const elem = (type , classNames = [], content = "", parent = document.body, prepend = false) => {
    const el = document.createElement(type);
    classNames.forEach(className => el.classList.add(className));
    el.appendChild(document.createTextNode(content));
    if (prepend) {
        parent.prepend(el);
    } else {
        parent.append(el);
    }
    return el;
}

const getUser = async (redirectOnFail = true, redirectOnSuccess = false) => {
    try {
        const user = await backend.fire("getUser");
        if (redirectOnSuccess) {
            window.location.href = "./dashboard.html";
        }
        return user;
    } catch (exception) {
        if (redirectOnFail) {
            window.location.href = "./index.html";
        }
        throw exception;
    }
}

Number.prototype.memoryString = function(base) {
    const symbols = ["B", "KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB", "YiB"];
    const basePos = symbols.indexOf(base) === 0? -Infinity: symbols.indexOf(base);
    const exp = (basePos !== -1)? basePos - 1: Math.floor(Math.log(this)/Math.log(1024));
    const loc = (exp < -1)? 0: exp + 1;

    return `${this == 0? "0": (this / Math.pow(1024, exp)).toFixed(2)} ${symbols[loc]}`;
}
Number.prototype.memory = function(base) {
    return this.memoryString(base).split(" ")[0];
}
Number.prototype.memoryWord = function(base) {
    return this.memoryString(base).split(" ")[1];
}

String.prototype.dateString = function() {
    const months = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    const date = new Date(parseInt(this) * 1000);
    if (!date) {
        return false;
    }

    let hour = date.getHours();
    hour = hour < 10? `0${hour}`: hour;
    let minute = date.getMinutes();
    minute = minute < 10? `0${minute}`: minute;

    return `${date.getDate()} ${months[date.getMonth()]} ${date.getFullYear()} ${hour}:${minute}`;
}

function getInputs(selectors, form) {
    return selectors.reduce((total, selector) => {
        const elem = form || document.querySelector("form");
        return { ...total, [selector]: elem.querySelector(`.input-container [name=${selector}]`).value};
    }, {});
}