load_values();

async function load_values() {
    user = await getUser();

    if (user.role !== "admin") {
        window.location.href = "./dashboard.html";
    }
}

const formElem = document.getElementById("add-user-form");
const formErrorElem = document.querySelector(".form-error");
const inputNames = ["username", "password", "role"];

formElem.addEventListener("submit", async (e) => {
    e.preventDefault();
    const values = getInputs(inputNames);

    formErrorElem.innerHTML = "";

    try {
        await backend.fire("createUser", values);

        window.location.href = "./user-list.html";
    } catch (exception) {
        console.log(exception);
        exception.split("\n").forEach((error) => {
            elem("DIV", [], error, formErrorElem);
        });
    }

});