const loginFormElem = document.getElementById("login-form");
const loginFormErrorElem = document.querySelector("#login-form .form-error");
const usernameElem = document.getElementById("username");
const passwordElem = document.getElementById("password")
;
const urlFormElem = document.getElementById("url-form");
const urlElem = document.getElementById("url");
const urlFormErrorElem = document.querySelector("#url-form .form-error");

urlElem.value = localStorage.getItem("baseURL");

// redirect to dashboard if already logged in
getUser(false, true);

loginFormElem.addEventListener("submit", async (e) => {
    e.preventDefault();
    loginFormErrorElem.textContent = "";
    
    try {
        const result = await backend.fire("login", { username: usernameElem.value, password: passwordElem.value });
        
        window.location.href = "./dashboard.html";
    } catch (exception) {
        loginFormErrorElem.textContent = exception;
    }
    
    return false;
});


urlFormElem.addEventListener("submit", async (e) => {
    e.preventDefault();
    try {
        localStorage.setItem("baseURL", urlElem.value);
        urlFormErrorElem.innerHTML = "Saved";
        window.location.reload();
    } catch(exception) {
        urlFormErrorElem.innerHTML = "Couldn't save the URL";
    }
});