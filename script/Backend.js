class Backend {
    endpoints = {
        login: {
            url: "/user/login.php",
            method: "POST",
        },

        logout: {
            url: "/user/login.php",
            method: "DELETE",
        },

        createUser: {
            url: "/user.php",
            method: "POST",
        },

        getUser: {
            url: "/user.php",
            method: "GET",
        },

        deleteUser: {
            url: "/user.php",
            method: "DELETE",
        },
        
        allUsers: {
            url: "/user/all.php",
            method: "GET",
        },

        allSystems: {
            url: "/system/all.php",
            method: "GET",
        },

        createSystem: {
            url: "/system.php",
            method: "POST",
        },

        shutdownSystem: {
            url: "/system/shutdown.php",
            method: "GET",
        },

        getSystem: {
            url: "/system.php",
            method: "GET",
        },

        deleteSystem: {
            url: "/system.php",
            method: "DELETE",
        },
    };

    fire = async (type, body = {}, getParams = {}) => {
        const time = new Date();
        const request = { ...this.endpoints[type] };
        if (getParams) {
            const qs = Object.keys(getParams)
                .map(
                    (k) =>
                        encodeURIComponent(k) +
                        "=" +
                        encodeURIComponent(getParams[k])
                )
                .join("&");
            request.url += qs ? `?${qs}` : "";
        }

        const headers = new Headers({
            "Content-Type": "application/json",
        });
        let requestBody = {
            method: request.method,
            headers,
        };

        if (request.method.toUpperCase() !== "GET") {
            requestBody.body = JSON.stringify(body);
        }

        let res, result;
        try {
            const baseURL = localStorage.getItem("baseURL");
            console.log(baseURL);
            res = await fetch(`${baseURL}${request.url}`, requestBody);
        } catch (exception) {
            throw "Couldn't send request";
        }

        try {
            result = await res.json();
            if (res.status < 200 || res.status > 299) {
                throw result;
            }
        } catch (exception) {
            if (exception.statusCode) {
                throw exception.message;
              } else {
                throw 'Server response invalid';
              }
        }

        return result.data;
    };
}
