const usersContainerElem = document.querySelector(".users-container");
const usersHeadingElem = document.querySelector(".users .heading");
const addUserButton = document.querySelector(".button.add-user");
const refreshButton = document.querySelector(".users .heading .retry");
let isUsersLoading = false;

load_values();

async function load_values() {
    const user = await getUser();

    if (user.role !== "admin") {
        window.location.href = "./dashboard.html";
    }

    fetchAndShowUsers();
}

refreshButton.addEventListener("click", () => {
    fetchAndShowUsers();
});

addUserButton.addEventListener("click", () => {
    window.location.href = "./user-add.html";
});

async function fetchAndShowUsers() {
    if (isUsersLoading) return false;
    
    document.querySelectorAll(".button.retry").forEach(el => el.disabled = true);
    
    isUsersLoading = true;
    usersHeadingElem.classList.add("loading");
    const users = await backend.fire("allUsers");
    usersHeadingElem.classList.remove("loading");
    usersContainerElem.innerHTML = "";
    if (users.length < 1) {
        usersContainerElem.innerHTML = "No users available";
    } else {
        users.forEach(showUser);
    }
    document.querySelectorAll(".button.retry").forEach(el => el.disabled = false);
    isUsersLoading = false;
}

function showUser(user) {
    const userElem = elem("DIV", ["user"], "", usersContainerElem);

    elem("DIV", ["username"], user.username, userElem);
    elem("DIV", ["role"], `Role: ${user.role.toUpperCase()}`, userElem);
    elem("DIV", ["created"], `Created on: ${user.created.dateString()}`, userElem);

    if (user.username !== "root") {
        elem("BUTTON", ["button", "button-theme"], "Delete", userElem).addEventListener("click", async () => {
            if (confirm(`Are you sure to delete '${user.username}'?`)) {
                try {
                    await backend.fire("deleteUser", {}, { username: user.username });
                    fetchAndShowUsers();
                } catch (exception) {
                    console.log(exception);
                }
            }
        });
    }
}