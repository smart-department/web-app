let user;
let isSystemsLoading = false;
load_values();

const logoutElem = document.getElementById("logout");
const actionsContainerElem = document.querySelector(".actions-buttons");
const systemsContainerElem = document.querySelector(".systems-container");
const systemsHeadingElem = document.querySelector(".systems .heading");


const nameElem = document.querySelector(".user-details .name");
const roleElem = document.querySelector(".user-details .role");
const createdElem = document.querySelector(".user-details .created");

async function load_values() {
    try {
        user = await getUser();

        nameElem.textContent = user.username;
        roleElem.textContent = `Role: ${user.role.toUpperCase()}`;
        createdElem.textContent = `Created on: ${user.created.dateString()}`;
        
        // do not show delete button if root is not the logged in user
        if (user.username !== "root") {
            const deleteButtonElem = elem("BUTTON", ["button", "button-theme"], "Delete Account", actionsContainerElem);
            deleteButtonElem.addEventListener("click", async () => {
                if (confirm(`Are you sure to delete your account '${user.username}'?`)) {
                    await backend.fire("deleteUser");
                    window.location.href = "./index.html";
                }
            });
        }
        
        // add admin specific buttons
        if (user.role === "admin") {
            const userListButtonElem = elem("BUTTON", ["button", "button-theme"], "User List", actionsContainerElem);
            userListButtonElem.addEventListener("click", async () => {
                window.location.href = "./user-list.html";
            });
            const addSystem = elem("BUTTON", ["button", "button-theme"], "Add System", actionsContainerElem);
            addSystem.addEventListener("click", async () => {
                window.location.href = "./system-add.html";
            });
        }

        fetchAndShowSystems();  
    } catch (exception) {
        console.error(exception);
    }
}

const refreshButton = document.querySelector(".systems .heading .retry");
refreshButton.addEventListener("click", () => {
    fetchAndShowSystems();
});

async function fetchAndShowSystems() {
    
    systemsContainerElem.querySelectorAll(".button.retry").forEach(el => el.disabled = true);    
    
    systemsHeadingElem.classList.add("loading");
    await noUIChangeFetch();
    systemsHeadingElem.classList.remove("loading");
}

async function noUIChangeFetch() {
    if (isSystemsLoading) return false;
    
    isSystemsLoading = true;
    const systems = await backend.fire("allSystems");
    
    systemsContainerElem.innerHTML = "";
    if (systems.length < 1) {
        systemsContainerElem.innerHTML = "No systems available";
    } else {
        systems.forEach(showSystem);
    }
    setTimeout(noUIChangeFetch, 2000);
    isSystemsLoading = false;
}

logoutElem.addEventListener("click", async () => {
    await backend.fire("logout");
    window.location.href = "./index.html";
});

function showSystem(el) {
    const systemElem = elem("DIV", ["system"], "", systemsContainerElem);

    elem("DIV", ["name"], el.name, systemElem, true);
    elem("DIV", ["status"], `Status: ${(el.info.message) || "UP"}`, systemElem);
    
    if (!el.info.message) {
        elem("DIV", ["memory"], `Load: ${el.info.load_average.join(" ")}`, systemElem);
        elem("DIV", ["memory"], `Memory: ${el.info.memory.used.memoryString()} / ${el.info.memory.total.memoryString()}`, systemElem);
    } else {
        elem("DIV", ["memory"], `Last Seen: ${el.last_update? el.last_update.dateString(): "Nil"}`, systemElem);
    }
    
    const systemActionElem = elem("DIV", ["actions"], "", systemElem);

    if (user.role === "admin") {
        // add more info button and delete button
        if (!el.info.message) {
            elem("BUTTON", ["button", "button-theme"], "More Info", systemActionElem).addEventListener("click", async () => {
                window.location.href = `./system.html?id=${el.id}`;
            });
        } else {
            elem("BUTTON", ["button", "button-theme", "retry"], "Retry", systemActionElem).addEventListener("click", async () => {
                fetchAndShowSystems();
            });
        }
        elem("BUTTON", ["button", "button-theme"], "Delete", systemActionElem).addEventListener("click", async function () {
            if (confirm(`Are you sure to delete '${el.name}'?`)) {
                await backend.fire("deleteSystem", {}, { id: el.id });
                fetchAndShowSystems();
            }
        });
    }
}