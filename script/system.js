let user, id;
let isSystemLoading = false;
let prevTimes = [];
let prevMemories = {
    ram: [],
    swap: []
};
let startTime = new Date();
let onceSuccess = false;
let once = false;
load_values();

const infoContainer = document.querySelector(".info-container");
const mainElem =  document.querySelector("main");
const statusElem = document.querySelector(".status");
const shutdownButton = document.getElementById("shutdown");

const maxNoOfPoints = 20;
const sliceIfGreater = (el) => el.length > maxNoOfPoints? el.slice(el.length - maxNoOfPoints): el;

let graphs = {
    memory: {
        title: "Memory",
        datasets: [
            {
                label: "RAM",
                color: "red"
            },
            {
                label: "Swap",
                color: "blue"
            },
        ],
        axesLabel: {
            x: "Time (s)"
        }
    },
    processor: {
        title: "Processor",
        datasets: [],
        axesLabel: {
            x: "Time (s)",
            y: "Clockrate (MHz)"
        }
    },
}

async function load_values() {
    user = await getUser();
    id = getParams && getParams.id;
    
    if (user.role !== "admin" || !id) {
        window.location.href="./dashboard.html";
    }

    shutdownButton.addEventListener("click", async () => {
        try {
            await backend.fire("shutdownSystem", {}, {id});
            window.location.href = "./dashboard.html";
        } catch (exception) {
            alert("couldn't shutdown system");
        }
    });

    fetchSystemData();
}

async function fetchSystemData() {
    document.querySelectorAll(".button.retry").forEach(el => el.disabled = true);       
    mainElem.classList.add("loading"); 
    await noUIUpdate();
    mainElem.classList.remove("loading");
    document.querySelectorAll(".button.retry").forEach(el => el.disabled = false);    
}

drawGraphs();

async function noUIUpdate() {
    try {
        const system = await backend.fire("getSystem", {}, { id });
        if (system.info.message) {
            throw {message: system.info.message, type: "backend", system};
        }
        statusElem.textContent = "Status: LIVE";
        displaySystemInfo(system);
        document.querySelectorAll(".button.retry").forEach(el => el.parentElement.removeChild(el));
        setTimeout(noUIUpdate, 1000);
    } catch (e) {
        console.error(e);
        if (e.type === "backend") {
            statusElem.textContent = `Status: ${e.message}`;
            if (!once) {
                once = true;
                showBasic(e.system, true);
            }
        } else if (typeof e === "string") {
            statusElem.textContent = `Status: ${e}`;
        }
    }
}

function displaySystemInfo(system) {
    if (!onceSuccess) { 
        onceSuccess = true;
        initSystem(system);
    }

    const memoryUnit = window.memoryGraph.options.scales.yAxes[0].scaleLabel.labelString;
    const ramVal = system.info.memory.ram.used.memory(memoryUnit);
    const swapVal = system.info.memory.swap.used.memory(memoryUnit);
    chartUpdate(window.memoryGraph, [ramVal, swapVal], `${Math.floor(((new Date()) - startTime) / 1000)}s`);
    chartUpdate(window.processorGraph, system.info.cpu.reduce((total, signleCpu) => [...total, ...signleCpu.clock_rate], []), `${Math.floor(((new Date()) - startTime) / 1000)}s`);
    updateEach(system);
}

function showBasic(system, showRetry = false) {
    const parent = document.querySelector(".basic .details");
    document.querySelector("h2.name").textContent = system.name;
    const createDetail = (text, parent) => elem("DIV", ["detail"], text, parent);
    
    createDetail(`Created on: ${system.created.dateString()}`, parent);
    createDetail(`Visibility: ${system.visibility.toUpperCase()}`, parent);
    const link = elem("A", ["detail"], "Visit info end-point", parent)
    link.href = `${system.endpoint}/info.php`;
    link.target = "_blank";
    link.rel = "noopener noreferr";

    if (showRetry) {
        elem("BUTTON", ["button", "retry"], "Retry", parent).addEventListener("click", () => {
            fetchSystemData();
        });
    }
}


function initSystem(system) {
    showBasic(system);
    infoContainer.style.display = "block";

    const label = Math.max(system.info.memory.ram.total, system.info.memory.swap.total).memoryWord();
    
    window.memoryGraph.options.scales.yAxes[0] = {
        ...window.memoryGraph.options.scales.yAxes[0],
        scaleLabel: {
            display: true,
            labelString: label
        },
        ticks: {
            min: 0,
            max: Math.ceil(Math.max(system.info.memory.ram.total, system.info.memory.swap.total).memory()),
            stepSize: Math.ceil(Math.max(system.info.memory.ram.total, system.info.memory.swap.total).memory()) / 10,
            display: false
        }
    };

    const containersNames = ["basicInformation", "overview", "memory", "processor", "diskUsage"];
    const cntr = Object.fromEntries(containersNames.map(el => { let elem = document.querySelector(`.${el} .details`); elem.innerHTML = ""; return[el, elem]; }));
    
    const createDetail = (text, parent, elemClass) => elem("DIV", ["detail", elemClass], text, parent);
    
    createDetail(`OS: ${system.info.meta.os} ${system.info.meta.machine_type}`, cntr.basicInformation);
    createDetail(`Release: ${system.info.meta.variant} ${system.info.meta.release}`, cntr.basicInformation);
    createDetail(`Hostname: ${system.info.meta.host}`, cntr.basicInformation);
    createDetail(`IP: ${system.info.meta.ip.replace("\n", ", ") || "Nil"}`, cntr.basicInformation);
    
    createDetail(`Load: ${system.info.load_average.join(" ")}`, cntr.overview);
    createDetail(`Uptime: ${system.info.uptime}`, cntr.overview);
    
    createDetail(`Memory: ${system.info.memory.ram.used.memoryString()} / ${system.info.memory.ram.total.memoryString()}`, cntr.memory, "memory-ram");
    createDetail(`Swap: ${system.info.memory.swap.used.memoryString()} / ${system.info.memory.swap.total.memoryString()}`, cntr.memory, "memory-swap");
    
    // CPU Details
    let cpuDatasets = [];
    system.info.cpu.forEach((cpu) => {
        createDetail(`Vendor: ${cpu.vendor}`, cntr.processor);
        createDetail(`Model: ${cpu.model}`, cntr.processor);
        createDetail(`Cores: ${cpu.cores}`, cntr.processor);
        createDetail(`Threads: ${cpu.threads}`, cntr.processor);
        [...Array(cpu.threads)].map((_, i) => {
            cpuDatasets.push({ label: `CPU - ${i + 1}` });
        });
        createDetail(`Clock Rate: ${cpu.clock_rate_average.toFixed(3)}MHz`, cntr.processor, "cpu-clockrate");
        if (cpu.clock_rate_max && cpu.clock_rate_min) {
            createDetail(`Clock Rate Range: ${cpu.clock_rate_min .toFixed(3)} - ${cpu.clock_rate_max.toFixed(3)} MHz`, cntr.processor);
        }
        
    });
    window.processorGraph.data.datasets = graphConfig(cpuDatasets, graphs.processor.title, graphs.processor.axesLabel).data.datasets;
    
    if (system.info.cpu[0].clock_rate_max && system.info.cpu[0].clock_rate_min) {
        window.processorGraph.options.scales.yAxes[0].ticks = {
            min: Math.min(system.info.cpu.map((el) => el.clock_rate_min || 0)),
            max: Math.max(system.info.cpu.map((el) => el.clock_rate_max || 0)),
            stepSize: (Math.min(system.info.cpu.map((el) => el.clock_rate_max || 0)) - Math.min(system.info.cpu.map((el) => el.clock_rate_min || 0))) / 10
        }
    }
        
    // Disk Usage
    console.log(system.info.disk)
    const diskTableElem = elem("TABLE", [], "", cntr.diskUsage);

    const rowHeadElem = elem("TR", [], "", diskTableElem);
    elem("TH", [], "Mounted", rowHeadElem);
    elem("TH", [], "File System", rowHeadElem);
    elem("TH", [], "Usage", rowHeadElem);
    
    system.info.disk.forEach((disk) => {
        const rowElem = elem("TR", [], "", diskTableElem);
        elem("TD", [], disk.mounted, rowElem);
        elem("TD", [], disk.filesystem, rowElem);

        const usageContainer = elem("TD", [], "", rowElem)
        const ProgressElem = elem("PROGRESS", [], "", usageContainer);
        ProgressElem.value = disk.used;
        ProgressElem.max = disk.available;

        elem("TD", [], `${disk.used.memoryString()} / ${disk.available.memoryString()}`, rowElem);
    });
}


function updateEach(system) {
    const memoryRamElem = infoContainer.querySelector(".memory-ram");
    const memorySwapElem = infoContainer.querySelector(".memory-swap");
    
    memoryRamElem.textContent = `Memory: ${system.info.memory.ram.used.memoryString()} / ${system.info.memory.ram.total.memoryString()}`;
    memorySwapElem.textContent = `Swap: ${system.info.memory.swap.used.memoryString()} / ${system.info.memory.swap.total.memoryString()}`;

    system.info.cpu.forEach((cpu, i) => {
        const cpuClockrateElem = infoContainer.querySelectorAll(".cpu-clockrate")[i];
        cpuClockrateElem.textContent = `Clock Rate: ${cpu.clock_rate_average.toFixed(3)}MHz`;
    });
}

function chartUpdate(chart, data = [], label) {
    data.forEach((el, i) => el && chart.data.datasets[i].data.push(el));

    const datas = chart.data.datasets.map(dataset => sliceIfGreater(dataset.data));
    chart.data.datasets.forEach((_, i, ar) => ar[i].data = datas[i] );

    chart.data.labels = sliceIfGreater([...chart.data.labels, label]);

    chart.update();   
}

function drawGraphs() {
    Object.keys(graphs).forEach((graphKey) => {
        const graph = graphs[graphKey];
        const ctx = document.getElementById(`graph-${graphKey}`).getContext("2d");

        const config = graphConfig(graph.datasets, graph.title, graph.axesLabel);
        window[`${graphKey}Graph`] = new Chart(ctx, config);
    });
}

function graphConfig(datasets, title, axesLabel = {}) {
    const colors = {
        red: {
            backgroundColor: "#E53E3E",
            borderColor: "#E53E3E99",
        },
        blue: {
            backgroundColor: "#3182CE",
            borderColor: "#3182CE99",
        },
        pink: {
            backgroundColor: "#D53F8C",
            borderColor: "#D53F8C99",
        },
        yellow: {
            backgroundColor: "#D69E2E",
            borderColor: "#D69E2E99",
        },
        green: {
            backgroundColor: "#38A169",
            borderColor: "#38A16999",
        },
        purple: {
            backgroundColor: "#805AD5",
            borderColor: "#805AD599",
        },
        teal: {
            backgroundColor: "#319795",
            borderColor: "#31979599",
        },
    };
    const defaultDataset = {
        label: "Label",
        fill: false,
    };

    const colorKeys = Object.keys(colors);
    return {
        type: "line",
        data: {
            labels: [],
            datasets: datasets.map((el, i) => { return Object.assign({},{ ...defaultDataset, data: [], ...colors[el.color || colorKeys[i % colorKeys.length] ], ...el }); })
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            title: {
                display: true,
                text: title
            },
            tooltips: {
                mode: "index",
                intersect: false,
            },
            hover: {
                mode: "nearest",
                intersect: true
            },
            elements: {
                line: {
                    tension: 0
                }
            },
            scales: {
                xAxes: [{
                    display: true,
                    ticks: {
                        stepSize: 3
                    },
                    scaleLabel: {
                        display: true,
                        labelString: axesLabel.x || ""
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: axesLabel.y || ""
                    }
                }]
            }
        }
    };
}